package com.cdi.tictactoe.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.cdi.tictactoe.R
import com.cdi.tictactoe.classes.Player
import com.cdi.tictactoe.databinding.FragmentMainMenuBinding

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class MenuFragment : Fragment() {
    private val playerOne = Player()
    private val playerTwo = Player()
    private var _binding: FragmentMainMenuBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentMainMenuBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonPlay.setOnClickListener {
            this.btnPlayClick()
        }

        binding.buttonSetupPlayerOne.setOnClickListener {
            this.btnSetupPlayers()
        }

        binding.buttonSetupPlayerTwo.setOnClickListener {
            this.btnCreatePlayers()
        }
    }

    private fun btnPlayClick() {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        Log.d("Shared pref ?", sharedPref.toString())

        var name : String =
            sharedPref!!.getString(getString(R.string.SHARED_PREF_PLAYER_ONE_NAME), "John")!!
        Log.d("players name : ", name)
        if (this.playerOne.getName().isEmpty() || this.playerTwo.getName().isEmpty()) {
            Log.d("btnPlayClickEventListener:", "players names are empty.")
        }
        findNavController().navigate(R.id.action_menuFragment_to_gameFragment)
    }

    private fun btnSetupPlayers() {
        // @TODO
        findNavController().navigate(R.id.action_menuFragment_to_setupFragment)
    }

    private fun btnCreatePlayers() {
        // @TODO
        findNavController().navigate(R.id.action_menuFragment_to_createPlayerFragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}