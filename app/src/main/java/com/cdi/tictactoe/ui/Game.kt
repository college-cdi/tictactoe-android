package com.cdi.tictactoe.ui

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.cdi.tictactoe.R
import com.cdi.tictactoe.classes.Player
import com.cdi.tictactoe.data.DBHelper
import com.cdi.tictactoe.data.model.PlayerModel
import com.cdi.tictactoe.databinding.FragmentGameBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class GameFragment : Fragment() {
    private var playerOne : Player = Player()
    private var playerTwo : Player = Player()
    private var activePlayer : Player = playerOne
    private var availableTiles: Int = 9
    private var winner: String = ""
    private lateinit var db : DBHelper

    private var _binding: FragmentGameBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.db = DBHelper(this.requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentGameBinding.inflate(inflater, container, false)
        this.init()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Binding event listeners
        binding.btnTile1.setOnClickListener {
            this.buttonClick(binding.btnTile1)
            //findNavController().navigate(R.id.action_menuFragment_to_gameFragment)
        }
        binding.btnTile2.setOnClickListener {
            this.buttonClick(binding.btnTile2)
        }
        binding.btnTile3.setOnClickListener {
            this.buttonClick(binding.btnTile3)
        }
        binding.btnTile4.setOnClickListener {
            this.buttonClick(binding.btnTile4)
        }
        binding.btnTile5.setOnClickListener {
            this.buttonClick(binding.btnTile5)
        }
        binding.btnTile6.setOnClickListener {
            this.buttonClick(binding.btnTile6)
        }
        binding.btnTile7.setOnClickListener {
            this.buttonClick(binding.btnTile7)
        }
        binding.btnTile8.setOnClickListener {
            this.buttonClick(binding.btnTile8)
        }
        binding.btnTile9.setOnClickListener {
            this.buttonClick(binding.btnTile9)
        }
        binding.btnReset.setOnClickListener {
            this.resetGameClick()
        }
    }

    private fun init() {
        if (this.playerOne.getName() == "") {
            val playerOne = this.getPlayerOne()
            this.playerOne.setName(playerOne.name)
            this.playerOne.setIcon(playerOne.icon)
        }
        if (this.playerTwo.getName() == "") {
            val playerTwo = this.getPlayerTwo()
            this.playerTwo.setName(playerTwo.name)
            this.playerTwo.setIcon(playerTwo.icon)
        }
        this.setScores()
    }

    private fun getPlayerOne(): PlayerModel {
        val sharedPref = activity?.getSharedPreferences(
            getString(R.string.SHARED_PREF_PLAYER_ONE_NAME), Context.MODE_PRIVATE)

        val name : String =
            sharedPref!!.getString(getString(R.string.SHARED_PREF_PLAYER_ONE_NAME), "Player 1")!!
        return getPlayerFromDB(name, R.drawable.cross)
    }

    private fun getPlayerTwo() : PlayerModel {
        val sharedPref = activity?.getSharedPreferences(
            getString(R.string.SHARED_PREF_PLAYER_TWO_NAME), Context.MODE_PRIVATE)

        val name : String =
            sharedPref!!.getString(getString(R.string.SHARED_PREF_PLAYER_TWO_NAME), "Player 2")!!
        val defaultIcon: Int = if (this.playerOne.getIcon() == R.drawable.circle) R.drawable.cross
        else R.drawable.circle
        return getPlayerFromDB(name, defaultIcon)
    }

    private fun getPlayerFromDB(playerName: String, defaultIcon: Int) : PlayerModel {
        val player = this.db.getPlayer(playerName)
        val icon = player?.icon ?: defaultIcon
        return PlayerModel(playerName, icon)
    }

    private fun buttonClick(view: View) {
        val buSelected: ImageButton = view as ImageButton
        var cellId = -1
        when(buSelected.id)
        {
            R.id.btn_tile1 -> cellId = 1
            R.id.btn_tile2 -> cellId = 2
            R.id.btn_tile3 -> cellId = 3

            R.id.btn_tile4 -> cellId = 4
            R.id.btn_tile5 -> cellId = 5
            R.id.btn_tile6 -> cellId = 6

            R.id.btn_tile7 -> cellId = 7
            R.id.btn_tile8 -> cellId = 8
            R.id.btn_tile9 -> cellId = 9
        }
        this.playTurn(cellId, buSelected)
    }

    private fun playTurn(cellId: Int, buSelected:ImageButton) {
        if (!buSelected.isEnabled || this.winner.isNotEmpty()) this.resetGame()
        buSelected.isEnabled = false

        buSelected.setBackgroundResource(this.activePlayer.getIcon())

        activePlayer.addCell(cellId)
        this.availableTiles--
        this.checkWinner()
    }

    private fun setScores() {
        val lblScorePlayer1: TextView = binding.scorePlayer1
        val lblScorePlayer2: TextView = binding.scorePlayer2
        lblScorePlayer1.text = getString(R.string.lbl_score_player1, this.playerOne.getName(), this.playerOne.getScore())
        lblScorePlayer2.text = getString(R.string.lbl_score_player2, this.playerTwo.getName(), this.playerTwo.getScore())
    }

    private fun invertActivePlayer() {
        this.activePlayer.setInactive()

        if (this.activePlayer == this.playerOne) {
            this.activePlayer = this.playerTwo
        } else {
            this.activePlayer = this.playerOne
        }
        this.activePlayer.setActive()
        val lblPlayerTurn : TextView = binding.lblPlayerTurn
        lblPlayerTurn.text = getString(R.string.lbl_player_turn, this.activePlayer.getName())
    }

    private fun resetGameClick() {
        this.resetGame()
    }


    private fun resetGame() {
        this.winner = String()
        this.availableTiles = 9
        val btn1: ImageButton = binding.btnTile1
        val btn2: ImageButton = binding.btnTile2
        val btn3: ImageButton = binding.btnTile3
        val btn4: ImageButton = binding.btnTile4
        val btn5: ImageButton = binding.btnTile5
        val btn6: ImageButton = binding.btnTile6
        val btn7: ImageButton = binding.btnTile7
        val btn8: ImageButton = binding.btnTile8
        val btn9: ImageButton = binding.btnTile9

        btn1.setBackgroundResource(androidx.cardview.R.color.cardview_light_background)
        btn1.isEnabled = true
        btn2.setBackgroundResource(androidx.cardview.R.color.cardview_light_background)
        btn2.isEnabled = true
        btn3.setBackgroundResource(androidx.cardview.R.color.cardview_light_background)
        btn3.isEnabled = true
        btn4.setBackgroundResource(androidx.cardview.R.color.cardview_light_background)
        btn4.isEnabled = true
        btn5.setBackgroundResource(androidx.cardview.R.color.cardview_light_background)
        btn5.isEnabled = true
        btn6.setBackgroundResource(androidx.cardview.R.color.cardview_light_background)
        btn6.isEnabled = true
        btn7.setBackgroundResource(androidx.cardview.R.color.cardview_light_background)
        btn7.isEnabled = true
        btn8.setBackgroundResource(androidx.cardview.R.color.cardview_light_background)
        btn8.isEnabled = true
        btn9.setBackgroundResource(androidx.cardview.R.color.cardview_light_background)
        btn9.isEnabled = true

        this.playerOne.resetCells()
        this.playerTwo.resetCells()
    }


    private fun checkWinner() {
        val activePlayerCells = this.activePlayer.getCells();

        for (i in 9 downTo 0 ) {
            if (( i == 1 || i == 4 || i == 7) && activePlayerCells.contains(i) && activePlayerCells.contains(i + 1) && activePlayerCells.contains(i + 2) ||
                activePlayerCells.contains(1) && activePlayerCells.contains(5) && activePlayerCells.contains(9) ||
                activePlayerCells.contains(3) && activePlayerCells.contains(5) && activePlayerCells.contains(7) ||
                activePlayerCells.contains(i) && activePlayerCells.contains(i+3) && activePlayerCells.contains(i+6)) {
                this.winner = this.activePlayer.getName()
                this.activePlayer.setScore()
                break
            } else if (this.availableTiles <= 0){
                val alertDialog = AlertDialog.Builder(this.context)
                alertDialog.setTitle(getString(R.string.gameResultTie))
                    .setPositiveButton(getString(R.string.positiveButton), null)
                alertDialog.show()
                this.resetGame()
                break
            }
        }

        this.setScores()

        if (this.winner.isNotEmpty()) {
            val alertDialog = AlertDialog.Builder(this.context)
            alertDialog.setTitle(getString(R.string.lbl_Winner, this.winner) )
                .setPositiveButton(getString(R.string.positiveButton), null)
            alertDialog.show()
            this.resetGame()
        } else {
            this.invertActivePlayer()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}