package com.cdi.tictactoe.ui

import SpinnerAdapter
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.core.view.get
import androidx.fragment.app.Fragment
import com.cdi.tictactoe.R
import com.cdi.tictactoe.classes.Icon
import com.cdi.tictactoe.data.DBHelper
import com.cdi.tictactoe.data.model.PlayerModel
import com.cdi.tictactoe.databinding.FragmentSetupBinding


class SetupFragment : Fragment() {
    private lateinit var db : DBHelper
    private var playersNameList : ArrayList<String> = ArrayList()
    private var playersIconList : ArrayList<Int> = ArrayList()
    private var items : ArrayList<PlayerModel> = ArrayList()
    private lateinit var sharedPrefPlayerOne : SharedPreferences
    private lateinit var sharedPrefPlayerTwo: SharedPreferences
    private lateinit var playerOne: PlayerModel
    private lateinit var playerTwo: PlayerModel
    private var _binding: FragmentSetupBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.db = DBHelper(this.requireContext())
        this.sharedPrefPlayerOne = requireActivity().getSharedPreferences(
            getString(R.string.SHARED_PREF_PLAYER_ONE_NAME), Context.MODE_PRIVATE)
        this.sharedPrefPlayerTwo = requireActivity().getSharedPreferences(
                getString(R.string.SHARED_PREF_PLAYER_TWO_NAME), Context.MODE_PRIVATE)
        val playerOneName = sharedPrefPlayerOne.getString(getString(R.string.SHARED_PREF_PLAYER_ONE_NAME), "Player 1")!!
        val playerTwoName = sharedPrefPlayerTwo.getString(getString(R.string.SHARED_PREF_PLAYER_TWO_NAME), "Player 2")!!
        this.playerOne = this.db.getPlayer(playerOneName)?: PlayerModel("Player 1", R.drawable.cross)
        this.playerTwo = this.db.getPlayer(playerTwoName)?: PlayerModel("Player 2", R.drawable.circle)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSetupBinding.inflate(inflater, container, false)

        binding.selectPlayerOne.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                val playerOneName = playersNameList[position]
                playerOne = db.getPlayer(playerOneName)!!
                val dropdownPlayerOneImage: Spinner = requireView().findViewById(R.id.select_player_one_image)
                var index = playersIconList.indexOf(playerOne.icon)
                dropdownPlayerOneImage.setSelection(index)
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        binding.selectPlayerOneImage.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                playerOne.icon = playersIconList[position]
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        binding.selectPlayerTwo.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                val playerTwoName = playersNameList[position]
                playerTwo = db.getPlayer(playerTwoName)!!
                val dropdownPlayerTwoImage: Spinner = requireView().findViewById(R.id.select_player_two_image)
                val index = playersIconList.indexOf(playerTwo.icon)
                dropdownPlayerTwoImage.setSelection(index)
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }


        binding.selectPlayerTwoImage.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                playerTwo.icon = playersIconList[position]
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.populateDropdowns()
        this.populateImageDropdown()
        this.setEventListeners()
        binding.submitPlayerOne.setOnClickListener {
            if (this.playerOne.name != this.playerTwo.name && this.playerOne.icon != this.playerTwo.icon) {
                this.submitPlayerOne()
                this.submitPlayerTwo()
            } else {
                Toast.makeText(
                    this@SetupFragment.requireContext(),
                    getString(R.string.error_player_selection),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun submitPlayerOne() {
        val playerOneNameSpinner: Spinner = requireView().findViewById(R.id.select_player_one)
        val playerOneIconSpinner: Spinner = requireView().findViewById(R.id.select_player_one_image)
        val playerOneName : String = playerOneNameSpinner.selectedItem.toString()
        val playerOneIcon: Int = Icon(this.requireContext()).getIcon(playerOneIconSpinner.selectedItemPosition)

        playerOne = PlayerModel(playerOneNameSpinner.selectedItem.toString(), playerOneIcon)

        this.db.updatePlayer(playerOne)

        with (sharedPrefPlayerOne.edit()) {
            putString(getString(R.string.SHARED_PREF_PLAYER_ONE_NAME), playerOneName)
            apply()
        }
    }

    private fun submitPlayerTwo() {
        val playerTwoNameSpinner: Spinner = requireView().findViewById(R.id.select_player_two)
        val playerTwoIconSpinner: Spinner = requireView().findViewById(R.id.select_player_two_image)
        val playerOneName : String = playerTwoNameSpinner.selectedItem.toString()
        val playerOneIcon: Int = Icon(this.requireContext()).getIcon(playerTwoIconSpinner.selectedItemPosition)

        playerTwo = PlayerModel(playerTwoNameSpinner.selectedItem.toString(), playerOneIcon)

        this.db.updatePlayer(playerTwo)

        with (sharedPrefPlayerTwo.edit()) {
            putString(getString(R.string.SHARED_PREF_PLAYER_TWO_NAME), playerOneName)
            apply()
        }
    }

    private fun populateDropdowns() {
        val dropdownPlayerOne: Spinner = requireView().findViewById(R.id.select_player_one)
        val dropdownPlayerTwo: Spinner = requireView().findViewById(R.id.select_player_two)

        this.items = this.db.readData()
        this.playersNameList = ArrayList()
        this.playersIconList = ArrayList()

        for (player in items) {
            this.playersNameList.add(player.name)
        }

        val playerNameAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this.requireContext(), android.R.layout.simple_spinner_item, playersNameList)
        playerNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        dropdownPlayerOne.setAdapter(playerNameAdapter)
        dropdownPlayerTwo.setAdapter(playerNameAdapter)
        if (this.playerOne.name != "Player 1") {
            val playerIndex = this.playersNameList.indexOf(this.playerOne.name)
            dropdownPlayerOne.setSelection(playerIndex)
        }
        if (this.playerTwo.name != "Player 2") {
            val playerIndex = this.playersNameList.indexOf(this.playerTwo.name)
            dropdownPlayerOne.setSelection(playerIndex)
        }
    }

    private fun populateImageDropdown() {
        val dropdownPlayerOneImage: Spinner = requireView().findViewById(R.id.select_player_one_image)
        val dropdownPlayerTwoImage: Spinner = requireView().findViewById(R.id.select_player_two_image)
        this.playersIconList = ArrayList()
        val iconTagList: ArrayList<String> = ArrayList()

        for (icon in Icon(this.requireContext()).getIcons()) {
            playersIconList.add(icon)
            iconTagList.add(Icon(this.requireContext()).getIconTag(icon))
        }

        val customAdapter = SpinnerAdapter(this.requireContext(), playersIconList, iconTagList)
        dropdownPlayerOneImage.setAdapter(customAdapter)
        dropdownPlayerTwoImage.setAdapter(customAdapter)

        dropdownPlayerOneImage.setSelection(this.playersIconList.indexOf(playerOne.icon))
        dropdownPlayerTwoImage.setSelection(this.playersIconList.indexOf(playerTwo.icon))
    }

    private fun setEventListeners() {

    }

}