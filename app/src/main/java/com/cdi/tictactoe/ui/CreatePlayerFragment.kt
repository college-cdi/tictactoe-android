package com.cdi.tictactoe.ui

import SpinnerAdapter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.Fragment
import com.cdi.tictactoe.R
import com.cdi.tictactoe.classes.Icon
import com.cdi.tictactoe.data.DBHelper
import com.cdi.tictactoe.data.model.PlayerModel
import com.cdi.tictactoe.databinding.FragmentCreatePlayerBinding


/**
 * A simple [Fragment] subclass.
 * Use the [CreatePlayerFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CreatePlayerFragment : Fragment() {
    private lateinit var db: DBHelper
    private var playerName = ""
    private var playerIcon = R.drawable.circle
    private var playersIconList : ArrayList<Int> = ArrayList()
    private var _binding: FragmentCreatePlayerBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.db = DBHelper(this.requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentCreatePlayerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.populateDropdowns()

        binding.submitPlayer.setOnClickListener() {
            this.createNewPlayer()
        }

        binding.selectPlayerImage.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                playerIcon = playersIconList[position]
                Log.d("Player Icon ID:", playerIcon.toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

    }

    private fun createNewPlayer() {
        val textplayerName = requireView().findViewById<EditText>(R.id.playerNameTextInput)
        this.playerName = textplayerName.text.toString()

        if (this.playerIcon != 0 && this.playerName != "") {
            val player = PlayerModel(this.playerName, this.playerIcon)
            this.db.insertPlayer(player)
        }
    }

    private fun populateDropdowns() {
        this.playersIconList = ArrayList()
        val dropdownPlayerImage: Spinner = requireView().findViewById(R.id.select_player_image)
        val iconTagList: ArrayList<String> = ArrayList()

        for (icon in Icon(this.requireContext()).getIcons()) {
            playersIconList.add(icon)
            iconTagList.add(Icon(this.requireContext()).getIconTag(icon))
        }

        val customAdapter = SpinnerAdapter(this.requireContext(), playersIconList, iconTagList)
        dropdownPlayerImage.setAdapter(customAdapter)

        dropdownPlayerImage.setSelection(this.playersIconList.indexOf(R.drawable.circle))
    }
}