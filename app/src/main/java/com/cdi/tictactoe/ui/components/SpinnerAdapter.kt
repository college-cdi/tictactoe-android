import com.cdi.tictactoe.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class SpinnerAdapter(var context: Context, var images: ArrayList<Int>, var imageName: ArrayList<String>) :
    BaseAdapter() {
    var inflater: LayoutInflater

    init {
        inflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun getItem(i: Int): Any {
        return images[i]
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    override fun getView(i: Int, paramView: View?, viewGroup: ViewGroup): View {
        var view = paramView
        view = inflater.inflate(R.layout.spinner_with_images, null)
        val icon = view.findViewById<View>(R.id.imageView) as ImageView
        val names = view.findViewById<View>(R.id.textView) as TextView
        icon.setImageResource(images[i])
        names.text = imageName[i]
        return view
    }
}