package com.cdi.tictactoe.data.model

data class PlayerModel(
    val name: String,
    var icon: Int,
)