package com.cdi.tictactoe.data

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import android.widget.Toast
import com.cdi.tictactoe.data.model.PlayerModel

const val DB = "TicTacToe"
const val playerTable : String = "Players"
const val colId = "Id"
const val colName = "Name"
const val colIcon = "Icon"

class DBHelper (private var context: Context) : SQLiteOpenHelper(context, DB, null, 1) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("CREATE TABLE $playerTable($colId INTEGER PRIMARY KEY AUTOINCREMENT, $colName varchar(50), $colIcon varchar(50));")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    fun deleteEverything() {
        Log.w("DB TRANSACTION:", "Deleting all data from $playerTable")
        val db = this.writableDatabase
        db.execSQL("DELETE FROM $playerTable;")
    }

    fun insertPlayer(playerModel: PlayerModel) {
        val db = this.writableDatabase
        val colInfo = ContentValues()
        colInfo.put(colName, playerModel.name)
        colInfo.put(colIcon, playerModel.icon.toString())

        val res = db.insert(playerTable, null, colInfo)
        if (res == (0).toLong()) {
            Toast.makeText(context, "Failed to insert new player.", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Successfully inserted new player.", Toast.LENGTH_SHORT).show()
        }
    }

    fun updatePlayer(playerModel: PlayerModel) {
        val db = this.writableDatabase
        val query = "UPDATE $playerTable SET $colIcon = ${playerModel.icon} WHERE $colName = ${playerModel.name};"
        val newValue = ContentValues().apply {
            put(colIcon, playerModel.icon)
        }
        val selection = "$colName = ?"
        val selectionArgs = arrayOf(playerModel.name)
        val res = db.update(playerTable, newValue, selection, selectionArgs )
        //val res = db.rawQuery(query, null)
        if (res <= 1.toLong()) {
                Log.d("Update res: ", res.toString())
        }
    }

    fun readData() : ArrayList<PlayerModel> {
        val list : ArrayList<PlayerModel> = ArrayList()
        val db = this.readableDatabase
        val query = "SELECT * FROM $playerTable"
        var playerName: String
        var playerIcon: String

        val res = db.rawQuery(/* sql = */ query, /* selectionArgs = */ null)
        if (res.moveToFirst()) {
            do {
                playerName = res.getString(res.getColumnIndexOrThrow(colName))
                playerIcon = res.getString(res.getColumnIndexOrThrow(colIcon))
                list.add(PlayerModel(playerName, playerIcon.toInt()))
            } while(res.moveToNext())
            res.close()
        }
        return list
    }

    fun getPlayer(playerName: String) : PlayerModel? {
        val db = this.readableDatabase
        val query = "SELECT * FROM $playerTable WHERE $colName = ? ;"

        val res = db.rawQuery(query, arrayOf(playerName))
        var player: PlayerModel? = null
        if (res.moveToFirst()) {
            do {
                player = PlayerModel(res.getString(res.getColumnIndexOrThrow(colName)), res.getString(res.getColumnIndexOrThrow(colIcon)).toInt())
            } while(res.moveToNext())
            res.close()
        }
        return player
    }

}