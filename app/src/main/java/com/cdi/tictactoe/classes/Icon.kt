package com.cdi.tictactoe.classes

import android.content.Context
import android.content.res.Resources
import com.cdi.tictactoe.R

/**
 * Class to hold all icon values.
 */
class Icon(context: Context) {
    private var list : ArrayList<Int> = ArrayList()
    private lateinit var context: Context

    init {
        this.context = context
        this.list.add(R.drawable.cross)
        this.list.add(R.drawable.circle)
        this.list.add(R.drawable.ship)
        this.list.add(R.drawable.skull)
    }

    internal fun getIcons(): ArrayList<Int> {
        return this.list
    }

    internal fun getIconTag(icon: Int): String {
        when(this.getIconById(icon)) {
            R.drawable.circle -> return this.context.resources.getString(R.string.circleImageTag)
            R.drawable.ship -> return this.context.resources.getString(R.string.shipImageTag)
            R.drawable.cross -> return this.context.resources.getString(R.string.crossImageTag)
            R.drawable.skull -> return this.context.resources.getString(R.string.skullImageTag)
            else -> {
                return ""
            }
        }
    }

    internal fun getIconById(Id: Int): Int {
        val index = this.getIndex(Id)
        return this.list[index];
    }

    internal fun getIcon(index: Int): Int{
        return this.list[index]
    }

    private fun getIndex(Id: Int): Int {
        return this.list.indexOf(Id);
    }
}