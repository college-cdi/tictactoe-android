package com.cdi.tictactoe.classes

import android.graphics.drawable.Drawable
import com.cdi.tictactoe.R

class Player() {
    private var score : Int = 0
    private var name: String = ""
    private var active: Boolean = false
    private var cells: ArrayList<Int> = ArrayList()
    private var Icon: Int = R.drawable.cross

    init {
        this.score = 0
    }

    internal fun getIcon(): Int {
        return this.Icon
    }

    internal fun setIcon(icon: Int) {
        this.Icon = icon
    }

    internal fun getName(): String {
        return this.name
    }

    internal fun setName(name: String) {
        this.name = name
    }

    internal fun isActive(): Boolean {
        return this.active
    }

    internal fun setActive() {
        this.active = true
    }

    internal fun setInactive() {
        this.active = false
    }

    internal fun getScore() : Int {
        return this.score
    }

    internal fun setScore() {
        this.score++
    }

    internal fun addCell(cellId: Int) {
        this.cells.add(cellId)
    }

    internal fun getCells() : ArrayList<Int> {
        return this.cells
    }

    internal fun resetCells() {
        this.cells.clear()
    }

}